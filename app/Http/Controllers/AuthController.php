<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('/register');
    }

    public function welcome(Request $request) 
    {
        $firstName = $request ['awal'];
        $lastName = $request ['akhir'];

        return view('welcome', compact('firstName', 'lastName'));
    }
}
