<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Registrasi</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="awal"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text" name="akhir"> <br> <br>

        <label>Gender:</label> <br> <br>
        <input type="radio" name="jk"> Male <br>
        <input type="radio" name="jk"> Female <br>
        <input type="radio" name="jk"> Other <br><br>


        <label>Nationality:</label> <br> <br>
        <select>
            <option value="Indonesian"> Indonesian </option>
            <option value="Singapore"> Singapore </option> 
            <option value="Malaysian"> Malaysian </option>
            <option value="Other"> Other </option>
            </select> <br><br>

        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>

        <label>Bio:</label> <br> <br>
        <textarea name="bio" id="" cols="50" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
      
    </form>
</body>
</html>